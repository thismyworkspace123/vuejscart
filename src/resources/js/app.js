import { createApp } from 'vue';
import Products from "./components/Products";
import Cart from "./components/Cart";

const productsApp = createApp(Products).mount("#products");

const cartApp = createApp(Cart).mount("#cart");
